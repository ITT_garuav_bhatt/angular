import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {

  @Input()list: any;
  @Output() eventEmitter = new EventEmitter();

  todoList =[] 
  constructor() { }

  ngOnInit(): void {
    this.todoList = this.list
  }

  delete(index: number) {
    this.eventEmitter.emit({key: 'delete-todo',  index})
  }

  edit (todo: any, index: number) {
    this.eventEmitter.emit({key: 'edit-todo',  data: {index, todo}})

  }
}
