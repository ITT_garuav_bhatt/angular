import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
 
})
export class HomeComponent implements OnInit {

  greet: string;
  todoItem: string;
  todoList: any = [] ;
  editID: number;
  constructor() { 
    this.greet = "hello home page using propety binding "
    this.todoItem = '';
    this.editID = -1;
  }

  ngOnInit(): void {
  }

  getString(): string {
      return 'called from html '
  }
  addItem() {
    console.log('this.todoItem = ', this.todoItem)
    if(this.todoItem.trim() !== '' ) {
      if(this.editID > -1) {
        this.todoList[this.editID].name = this.todoItem
        } else {
          this.todoList.push({name: this.todoItem, id: Math.round(Math.random() * 10)})
        }
        this.todoItem = '';
        this.editID = -1;
    }
   
  }
  delete(index : number) {
    this.todoList.splice(index,1)
  }

  edit(todo: any, index: number ) {
    this.todoItem = todo.name
    this.editID = index
  }

  emittedEvents(event: any) {
    console.log('event = ',event)
    if (event.key === 'delete-todo') {
        this.delete(event.index)
    } else if(event.key === 'edit-todo') {
      const {todo, index} = event.data //de-structuring 
      this.edit(todo, index)
    }
  }

}
