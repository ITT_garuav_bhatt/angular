import { Component, OnInit } from '@angular/core';
import { PostService } from 'src/app/services/post/post.service';
import {filter, map } from 'rxjs/operators'
@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  postList = []
  constructor(private postService: PostService) { }

  ngOnInit(): void {
   this.postService.getPosts().subscribe( res => {
      console.log('posts',res)
      this.postList = res
     }, error => {
      console.log('error ',error)
     }, () => {
      console.log('completed')
     })
  }

}
